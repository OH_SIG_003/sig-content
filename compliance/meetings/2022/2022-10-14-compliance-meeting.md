# 合规SIG例会 2022-10-14 15:30-16:30(UTC+08:00)Beijing

## 议题Agenda
- OSS Compliance in Onrio      Piana，Alberto，Rahul

## 与会人Attendees
- Oniro：   Piana，Alberto，Rahul、 Jaroslaw Marek (Jarek)、  Davide Ricci、  Wangke (Michael Wang) 、

- Compliance-SIG:   陈雅旬、高琨、郑志鹏、丛林、余甜、高亮、

## 纪要Notes
- 议题1、OSS Compliance in Onrio

会议结论：

1、 Process for Managing and resolving compliance issue

1） Compliance Issues are managed in the private repo which is mirrored from oniro repo

2） Technical meetings with developers and anction items for developers may be reflected in main project repo

3） Third party components compliance issue will be raise in upstream repo

2、 Certification：openchain

1） openchain is a International Standard for open source license compliance

2） openchain use another Standard which called spdx that help telling what is in the package in machine readable way

3） Quality system should adapt your organization implementing the Standard, then find out whether you are compliance ， you can also have the third party to certify your conform

3、 TOOLs：

1）Fossology support human validation of automated license scanner results， to fix false positives and false negatives， and detect possible compliance issue

2） Audit is an asyincronous process，should flow in parallel with development

4、Demo: Example of  A component clearing in Fossology

      1）Audit Policy for OSTC  

https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/docs/-/blob/main/audit_workflow/oniro_ip_audit_guidelines.md

      2） Binary file 、 License and Copyright Identify  in  Fossology   https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/docs

      3） example of compliance issue which found in OpenHarmony 3.0 (Result of  audit of OpenHarmony-3.0-LTS)

5、 Frequency Controversial issues

1）hardware support、 patents on audio/video codecs etc 、License incompatibilities、 copyright and patent trolls

6、Reuse Third Party work

      1） upstream first

      2） if upstream doesn’t  accept our changes, please clear separation between upstream sources and downstream changes( original package+ patches folder   or  forking and  correctly branching)  


