# July.5, 2022 at 10:00am GMT+8

## Agenda
|时间|议题|发言人|
|--|--|--|
|10:00-10:15|Mips编译工具和框架适配进展同步|袁祥仁 李兵|
|10:15-10:25|Mips系统三方组件适配进展同步|Lain 李兵|
|10:25-10:35|OpenHarmony编译工具链开源进展分享|黄慧进 袁祥仁 Lain|
|10:35-10:40|子系统适配进展同步|袁祥仁|

## Attendees
- [@hongtao6573](https://gitee.com/hongtao6573)
- [@cip_syq](yunqiang.su@oss.cipunited.com)
- [@Lain]()
- [@wicom](https://gitee.com/wicom)
- [@wangxing-hw](https://gitee.com/wangxing-hw)
- [@libing23](https://gitee.com/libing23)
- [@huanghuijin](https://gitee.com/huanghuijin)
- [@huangsox](https://gitee.com/huangsox)
- [@liujk000](https://gitee.com/liujk000)


## Notes

#### 议题一、Mips编译工具和框架适配进展同步

**结论**
- 1.使用clang可以编译处系统镜像文件，采用glibc库时板子可以启动，使用musl-libc时启动失败。分析原因是编译工具链制作时混用了musl-libc和glibc库，因OH版clang已经开源，适配任务切换到其版本上，此问题暂不影响后续工作。

**存在问题：**
- 问题1：编译框架适配中对于__sync_fetch_and_add_8系列的原子操作函数处理不确定是否合理。
- 结论：问题的原因是mips32平台下没有这个系列函数的实现，需要用atomic库函数实现，详细情况在微信群沟通。


#### 议题二、Mips系统三方组件适配进展同步

**结论**
- 1.系统三方组件适配需求识别已完成，结果表格会发至共建群，下一步根据表格联系共建方认领需求。


#### 议题三、OpenHarmony编译工具链开源进展分享

**结论**
- OpenHarmony编译工具链已开源，可在其基础上添加mips平台支持，仓库地址已发出。

**存在问题**
- 问题1：在OH版本llvm+clang中添加扩展的汇编指令和simd指令流程是怎样的。
- 结论：OH版本llvm和clang仓库已开源，按照社区正常的流程提交issue，pr经测试审查后可合入主干。

#### 议题四、子系统适配进展同步

**结论**
- 当前在适配histreamer组件，已完成50%。

## Action items
