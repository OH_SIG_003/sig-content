#   January 4, 2023 at 16:00am-17:30am GMT+8

## Agenda

| 时间        | 议题                                             | 发言人 |
| ----------- | ------------------------------------------------ | ------ |
| 16:00-16:10 | 议题一、Media的codec驱动能力开发进展。           | 张国荣 |
| 16:10-16:25 | 议题二、驱动子系统Audio模块新增HDI接口参数评审。 | 张云虎 |
| 16:25-16:35 | 议题三、驱动子系统Display模块新增HDI接口评审。   | 朱思远 |
| 16:35-16:50 | 议题四、驱动子系统Vibrator模块新增HDI接口。      | 马楠   |

## Notes

- **议题一、Media的codec驱动能力开发进展----张国荣，翟海鹏，刘飞虎，张浩东** 

  议题进展： Codec HDI1.0版本新增支持mpeg4,vp8,vp9,hevc格式编码的不同分辨率, setParameter参数设置优化工作 完成进度70%。

- **议题二、驱动子系统Display模块新增HDI接口评审----赵文华，袁博，刘飞虎，朱思远，刘洪刚，翟海鹏，徐羽琼，胡军剑、刘增辉、韩莹、林洪亮、 黄一宏** 

  议题结论：

  1、对display模块变更HDI接口和数据类型进行评审，变更1个接口，新增枚举值3个，接口定义符合合规、兼容性和扩展性要求，接口评审通过 。

  2、变更接口列表

  | 变更前接口                                                   | 变更后接口                                                   | 评审结论 |
  | ------------------------------------------------------------ | ------------------------------------------------------------ | -------- |
  | int32_t SetTransformMode(uint32_t devId, uint32_t layerId, TransformType type) | int32_t SetLayerTransformMode(uint32_t devId, uint32_t layerId, TransformType type | 通过     |

- **议题三、驱动子系统vibrator和sensor模块新增HDI接口评审----赵文华，袁博，刘飞虎、刘洪刚，惠月红，马楠、 黄一宏** 

  议题结论：对vibrator和sensor模块新增HDI接口和数据类型进行评审，新增4个接口和5个结构体，接口定义符合合规、兼容性和扩展性要求，接口评审通过。

  | 变更前接口                                                   | 接口描述                   | 评审结论 |
  | ------------------------------------------------------------ | -------------------------- | -------- |
  | EnableCompositeEffect([in] HdfVibratorEffect VibratorEffect) | 使能组合定义振动效果       | 通过     |
  | IsHapticRunning([out] bool state)                            | 当前系统是否在执行振动效果 | 通过     |
  | getEffectInfo([in] String effectType, [out] struct HdfEffectInfo[] effectInfo) | 获取振动效果信息           | 通过     |
  | ReadData([out] struct HdfSensorEvents[] event)               | 读取sensor事件数据         | 通过     |

 会议通知：( https://lists.openatom.io/hyperkitty/list/sig_driver@openharmony.io/thread/35SYSYZBP25GSLLZ3V6IV4VRY3T7AP4G/)

 会议议题申报：(https://shimo.im/sheets/36GKhpvrXd8TcQHY/AdiEd) 

 Driver_sig邮件列表订阅:（https://lists.openatom.io/postorius/lists/sig_driver.openharmony.io/ ）

（操作方法<https://gitee.com/openharmony/drivers_framework/issues/I45X7P?from=project-issue>） 