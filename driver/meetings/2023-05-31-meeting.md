#   May 31, 2023 at 15:00am-16:00am GMT+8

## Agenda

| 时间        | 议题                                     | 发言人 |
| ----------- | ---------------------------------------- | ------ |
| 15:00-15:10 | 议题一、传感器共建需求进展               | 于敏杰 |
| 15:10-15:20 | 议题二、Codec模块共建需求评审            | 祁金全 |
| 15:20-15:40 | 议题三、display模块LTPO新增hdi接口评审。 | 杨航   |

## Notes

- **议题一、传感器共建需求进展----刘飞虎，于敏杰，马楠 **

   议题纪要：已完成温湿度传感器设备驱动开发，计划6月20日验收。

- **议题二、Codec模块共建需求进展和新增HDI接口评审----翟海鹏，刘飞虎，张浩东，祁金全、张国荣**

  议题结论：1、对codec模块图片编解码共建需求已完成开发，计划6月20日验收。

   2、接口定义：https://gitee.com/openharmony/drivers_interface/pulls/369/

-  **议题三、display模块LTPO新增hdi接口评审----赵文华，刘飞虎、刘洪刚、翟海鹏 、李兵、杨航、梁艺冰、卢夏衍**

   议题结论：新增LTPO子模块新增3个HDI接口，结构体定义符合合规、兼容性和扩展性要求，结构体评审通过。 

  SetDisplayMode接口为原生接口，增加帧率回调。 

  GetDisplayVblankPeriod接口获取vsync周期时长，单位ns RegSeamlessChangeCb接口注册可平滑切换的hook

 会议通知：(https://lists.openatom.io/hyperkitty/list/sig_driver@openharmony.io/thread/ESYFKBKSBOELIAEXD5JGMAUHY4NAU7MO/)

 会议议题申报：(https://shimo.im/sheets/36GKhpvrXd8TcQHY/AdiEd) 

 Driver_sig邮件列表订阅:（https://lists.openatom.io/postorius/lists/sig_driver.openharmony.io/ ）

（操作方法<https://gitee.com/openharmony/drivers_framework/issues/I45X7P?from=project-issue>） 