#   March 22, 2023 at 16:00am-17:30am GMT+8

## Agenda

| 时间        | 议题                                                         | 发言人         |
| ----------- | ------------------------------------------------------------ | -------------- |
| 16:00-16:10 | 议题一、传感器共建需求进展。                                 | 于敏杰         |
| 16:10-16:20 | 议题二、Codec模块共建需求评审。                              | 祁金全         |
| 16:20-16:40 | 议题三、location模块新增低功耗围栏HDI接口评审。              | 朱焱           |
| 16:40-17:00 | 议题四、音频模块新增低时延接口参数和MMAP文件描述符类型变更评审。 | 金俊文，张云虎 |

## Notes

- **议题一、传感器共建需求进展----刘飞虎，于敏杰，马楠 **

   议题纪要：已完成温湿度传感器设备驱动开发，待通过门禁和测试验证后，提交上库。 

- **议题二、Codec模块共建需求进展----翟海鹏，刘飞虎，张浩东，祁金全、张国荣**

  议题结论：

  1、jpeg图片硬解码已完成芯片层的适配工作，下周开始与HDI进行联调。

  2、HDI1.0枚举类型扩展，新增ParamKey枚举类型新增三个值，读取对应的buffer数量。

-  **议题三、location模块新增围栏子模块HDI接口评审----赵文华，刘飞虎，刘洪刚、翟海鹏，朱焱**

   议题结论：

  1、对围栏模块为新增子模块，新增数据结构和HDI接口进行评审，接口定义符合合规、兼容性和扩展性要求，版本号为1.0，接口和结构体评审通过。

   2、接口和数据结构定义：https://gitee.com/openharmony/drivers_interface/pulls/295。
  
- **议题四、audio模块新增接口和数据类型评审----赵文华，刘飞虎，刘洪刚， 金俊文，张云虎**

   议题结论：

   1、对Audio模块新增1个接口和1个结构体元素变更进行评审，接口定义符合合规、兼容性和扩展性要求，版本号升级为1.1，接口和结构体评审通过。            2、新增render和capture的低时延场景接口：GetMmapPosition([out] unsigned long frames, [out] struct AudioTimeStamp time)；MMAP文件描述符类型struct AudioMmapBufferDescriptor变更

 会议通知：(https://lists.openatom.io/hyperkitty/list/sig_driver@openharmony.io/thread/FVKS5PCHCTTJ42VJOQECFEMSUJAEOWAR/)

 会议议题申报：(https://shimo.im/sheets/36GKhpvrXd8TcQHY/AdiEd) 

 Driver_sig邮件列表订阅:（https://lists.openatom.io/postorius/lists/sig_driver.openharmony.io/ ）

（操作方法<https://gitee.com/openharmony/drivers_framework/issues/I45X7P?from=project-issue>） 