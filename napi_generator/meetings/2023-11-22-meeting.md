## Agenda

- OH 辅助工具 SIG例会

  |    时间     | 议题                       | 发言人                       |
  | :---------: | -------------------------- | ---------------------------- |
  | 17:00-17:50 | 议题一、napi_generator业务 | 刘鑫，苏重威，赵军霞，苟晶晶 |

## Attendees

- [@zhaojunxia2020](https://gitee.com/zhaojunxia2020)
- [@gou-jingjing](https://gitee.com/gou-jingjing)
- [xliu-huanwei](https://gitee.com/xliu-huanwei)
- [@su-chongwei](https://gitee.com/su-chongwei)

## Notes

- 会议内容：

  1. 澄清napi工具当前主要做的功能：

      1.1 基于qqdemo适配功能，讨论注册回调为object形式时如何实现；
  
      1.2 对比原qqdemo，工具为适配对.d.ts文件做了什么改动。

  2. 工具优化需求

      2.1 工具对不支持的.d.ts格式报错后增加处理，FAQ中增加当前已知不支持的推荐方案

      2.2 新增工具当前支持能力链接—链接到已支持能力release-notes

      2.3 梳理用例与特性关系文档

  ## Action items
  
  1、修改demo适配文档并提交  -- 赵军霞、苟晶晶
  
  2、根据提出的工具优化需求对工具进行优化 --赵军霞、苟晶晶
  
  