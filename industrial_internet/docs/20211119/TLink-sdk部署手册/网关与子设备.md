### 网关与设备

包含文档步骤说明以及操作说明，其中步骤说明便于理解，操作说明则是具体的使用以及需要修改的部分。运行结果则输出了程序运行出来的效果。

#### 操作步骤

##### 步骤一：配置依赖以及日志

```c
tiot_set_print_callback(test_print_logcb);
tiot_sysdep_set_portfile(&g_tiot_sysdep_portfile);
```

##### 步骤二：配置三元组信息、认证建连以及配置消息接收以及事件接收句柄

网关与子设备采用与普通直连设备一样的方式，只需要设置三元组信息，然后调用`tiot_mqtt_connect`进行服务器认证成功建立连接即可。

对接收的数据或者响应的事件做响应的处理就进行配置。比如接收回调函数会在接收到消息之后进行触发。

其中`void *tiot_mqtt_init_ctx(tiot_mqtt_init_config_t *device_config, void *mqtt_recv_ctx, void *mqtt_event_ctx)`的三个参数说明如下：

| 参数             | 意义                                                      |
| ---------------- | --------------------------------------------------------- |
| `device_config`  | 包含`device_name,product_key,device_secret,host,port`的值 |
| `mqtt_recv_ctx`  | 消息接收的句柄                                            |
| `mqtt_event_ctx` | 事件接收的句柄                                            |

```c
tiot_mqtt_init_config_t mq_cfg = {
    .product_key       = "I08UhoUWo",
    .device_name       = "i0qnhqnZq",
    .device_secret     = "f4e0b684c42d9b7a"
};
char *src[] = {mq_cfg.product_key, mq_cfg.device_name};
mq_process = tiot_mqtt_init_ctx(&mq_cfg, test_mqtt_default_recv_ctx, test_mqtt_event_ctx);
```

##### 步骤三：子设备MQTT动态注册

子设备在需要在动态注册的topic发布请求来获取设备的三元组消息，服务器验证通过之后会将响应的值通过指定的topic发送下来，所以我们还需订阅动态注册的响应topic用来接收响应的数据。

注意：==设备发送的device_info是设置端随意指定的一个变量，真正的device_name是需要通过云端的响应来进行获取，该值在响应的json结构中有所体现。==

###### 1. 设置子设备变量

获取子设备信息，并添加进`g_subdev`变量。在动态注册过程中会遍历读取进对应消息，`g_subdev`变量由以下结构组成：

```c
/ * 子设备信息 */
typedef struct {
    char *product_key;
    char *device_info;
} tiot_subdev_dev_t;
```

###### 2. 动态注册请求topic以及发布内容

需要向指定的topic发送请求消息，其中请求的topic以及消息内容格式如下：

1. topic格式

   `tlink/${productKey}/${deviceName}/sub/register_reply`

   示例：`persistent://T0001/default/8725dfbe93a64c5db1653ca766c036ce/8e39687812a643b597bf7309e43b41d3/sub/register_reply`

2. 发布内容格式

   ```json
   {
           "reqid":        "1637152359116",
           "v":    "1",
           "t":    1637152359116,
           "method":       "sub.register",
           "spec": {
                   "ack":  0
           },
           "data": {
                   "deviceName":   "111",
                   "productKey":   "X026hP6WP"
           }
   }
   ```

###### 3. 动态注册响应topic以及消息的处理

服务端接收到请求消息之后会回复对应的响应消息，此时需要订阅相关topic用来接收数据，其中订阅的topic格式如下：

1. 需要订阅的topic：`tlink/${productKey}/${deviceName}/sub/register_reply`

   示例：`tlink/8725dfbe93a64c5db1653ca766c036ce/8e39687812a643b597bf7309e43b41d3/sub/register_reply`

2. 接收到的响应数据格式：

   ```json
   {
           "deviceSecret": "0513871d5599749d",
           "productKey":   "X026hP6WP",
           "deviceName":   "20Eyiay1a",
           "deviceUniqueNo":       "111"
   }
   ```

动态注册之后就会得到索引device_info在平台对应的device_name以及device_secret

![image-20211117172635742](png/动态注册三元组信息.png)

当接收到子设备的三元组消息之后就可以需要对子设备结构体中的初始化内容进行修改，将device_info改为实际的device_name值(因为后续的操作都需要是在云平台已经存在的device_name)，如下所示：

```c
tiot_subdev_dev_t g_subdev[] = {
    {
        "X026hP6WP",
        "d0jphXpZX"
    }
};
```

##### 步骤四：子设备退网

子设备退网指的是子设备从已有的拓扑结构中进行退出的操作。

###### 1. 子设备退网请求topic以及发布内容

需要向指定的topic发送请求消息，其中请求的topic以及消息内容格式如下：

1. topic格式

   ​	`tlink/${productKey}/${deviceName}/topo/delete`	

   示例：`tlink/I08UhoUWo/i0qnhqnZq/topo/delete`

2. 发布内容格式

   ```json
   {
     "reqid": "0020fdf71f0d491da12ba3cd38b205be",
     "v": "1",
     "t": 1630054074378,
     "method": "topo.delete",
     "spec":{
         "ack":0
     },
     "data": {
       "deviceName": "deviceName1234",
       "productKey": "a1234******"
     }
   }
   ```

发布退网请求消息成功之后可以在平台看到被请求的子设备已经与网关不保持拓扑关系了。

![image-20211117203415243](png/子设备退网.png)

##### 步骤五：子设备上线

###### 1. 子设备上线请求topic以及发布内容

需要向指定的topic发送请求消息，其中请求的topic以及消息内容格式如下：

1. topic格式

   `tlink/${productKey}/${deviceName}/sub/login`

   示例：`tlink/8725dfbe93a64c5db1653ca766c036ce/8e39687812a643b597bf7309e43b41d3/sub/login`

2. 发布内容格式

   ```json
   {
           "reqid":        "1637148660125",
           "v":    "1",
           "t":    1637148660125,
           "method":       "sub.login",
           "spec": {
                   "ack":  0
           },
           "data": [{
                           "productKey":   "X026hP6WP",
                           "deviceName":   "d0jphXpZX",
                           "sign": "99C4E8A19C8C998969CDD09541227DCD2A0C0B861126E9CE816A38F9FF1AA06B",
                           "signmethod":   "hmacsha256",
                           "timestamp":    "1637148660125"
                   }, {
                           "productKey":   "X026hP6WP",
                           "deviceName":   "20Eyiay1a",
                           "sign": "99C4E8A19C8C998969CDD09541227DCD2A0C0B861126E9CE816A38F9FF1AA06B",
                           "signmethod":   "hmacsha256",
                           "timestamp":    "1637148660125"
                   }]
   }
   ```

当子设备之后就能够在平台上看到子设备的状态改为在线。

![image-20211022083543846](png/子设备在线.png)

##### 步骤六：子设备下线

###### 1. 子设备下线请求topic以及发布内容

需要向指定的topic发送请求消息，其中请求的topic以及消息内容格式如下：

1. topic格式：`tlink/${productKey}/${deviceName}/sub/logout`

   示例：`tlink/I08UhoUWo/i0qnhqnZq/sub/logout`

2. 发布内容格式

   ```json
   {
           "reqid":        "1637151732027",
           "v":    "1",
           "t":    1637151732027,
           "method":       "sub.logout",
           "spec": {
                   "ack":  0
           },
           "data": [{
                           "productKey":   "X026hP6WP",
                           "deviceName":   "d0jphXpZX"
                   }, {
                           "productKey":   "X026hP6WP",
                           "deviceName":   "20Eyiay1a"
                   }]
   }
   ```

当子设备发出下线请求就能够在平台中看到在线状态改为离线。在平台查看状态如下：

![子设备离线](png/子设备离线.png)

##### 步骤七：退出并销毁相关资源

程序结束需要进行mqtt相关资源以及进程资源的销毁。

```c
/* 销毁MQTT实例 */
res = tiot_mqtt_deinit(&mqtt_handle);
if (res < RET_SUCCESS)
{
printf("tiot_mqtt_deinit failed: -0x%04X\n", -res);
return -1;
}

g_tiot_sysdep_portfile.platform_task_join(&g_mqtt_process_thread, NULL);
g_tiot_sysdep_portfile.platform_task_join(&g_mqtt_recv_thread, NULL);
```

#### 使用说明

这里主要说明了需要对程序demo进行替换的值。

1.设置子设备变量值

```c
/**子设备拓扑结构
 * 
 * char *product_key;
 * 
 * char *device_name;
 * 
 */
tiot_subdev_dev_t g_subdev[] = {
    {
        "M0eGgpGpp",
        "gate_child_device01"
    }
};
```

2.网关以及端口的设置

```c
char *url = "121.37.143.77";
char host[100] = {0};
uint16_t port = 10003;
```

2.网关三元组的设置

```c
/* 设备的三元组认证信息 */
char *product_key = "z0l1go1po";
char *device_name = "gate_device01";
char *device_secret = "c3d9b5ff0e7233b9";
```

3.程序中动态注册、拓扑添加、子设备上下线等分为不同得程序的程序块，当需要使用某一块功能就将注释去掉。比如动态注册如下：

需要替换对应的topic

```c
/* mqtt订阅MQTT动态注册响应消息 */
    {
        char *sub_topic = "tlink/I08UhoUWo/i0qnhqnZq/sub/register_reply";

        res = tiot_mqtt_sub(mqtt_handle, sub_topic, NULL, 1, NULL);
        if (res < 0)
        {
            printf("tiot_mqtt_sub failed, res: -0x%04X\n", -res);
            return -1;
        }
    }

    /* mqtt发布MQTT动态注册消息 */
    {
        char *pub_topic = "tlink/I08UhoUWo/i0qnhqnZq/sub/register";
        /* 修改成子设备的相关信息 */
        char *pub_payload = "{\"deviceName\":\"gate_child_device01\",\"productKey\":\"M0eGgpGpp\"}";

            res = tiot_mqtt_pub_with_method(mqtt_handle, pub_topic, (uint8_t *)pub_payload, (uint32_t)strlen(pub_payload), 0, "sub.register");
            if (res < 0)
            {
                printf("tiot_mqtt_sub failed, res: -0x%04X\n", -res);
                return -1;
            }
    }
```

#### 运行结果

以下是子设备动态注册的输出：

![image-20211117172635742](png/子设备动态注册.png)

如需调试可参考《sdk操作步骤》文档，使用mqttx进行收发消息的调试。

