# Oct 11, 2022 at 15:00pm GMT+8

## Agenda
+ 遗留问题汇总
+ 知识体系联合立项进展
+ Codelabs开发样例上官网评审
+ 知识赋能版权讨论
+ DevEco Device Tool赋能课程

## Attendees
- [zeeman_wang](https://gitee.com/zeeman_wang)
- [张路](zhanglu@iscas.ac.cn)
- [madixin](https://gitee.com/madixin)
- [Cruise2019](https://gitee.com/Cruise2019)
- [深开鸿-guoyuefeng](guoyuefeng@kaihongdigi.com)
- [深开鸿-孙鑫(深开鸿-孙鑫)]()
- [博泰-陆冕]()
- [软件所-朱伟]()
- [华为-hepeipei]()
- [润和-连志安]()
- [华为-hujie]()  
## Notes
#### 议题一、知识体系联合立项进展（张前福）  
**进展**  
目前博泰、美的、深开鸿、九联、润和、软件所等基本都基于自己的联合立项项目申报了开发样例用于本次华为HDC生态共建展示。同时欢迎社区继续申报样例，截止时间2022年10月14日。  
展示样例总体要求如下:  
+ 针对的是开发态的，非商业产品展示
+ 开发样例要有互动性，能够和开发者做互动
+ 开发样例能够体现OpenHarmony相关技术特性（包含原生特性、共建特性、三方组件等）  

#### 议题二、Codelabs开发样例上官网评审（hepeipei）
**进展**   
完成了15个Codelabs样例：  
+ 为应用添加运行时权限（eTS): RK3568, 3.1 Release, 实现了动态权限申请示例
+ Web组件加载本地H5小程序（eTS）:RK3568, 3.1 Release， Web组件加载H5小程序
+ 像素转换（eTS）: RK3568, 3.2Beta1, 展示像素单位与像素转换的使用方式
+ ArkUI常用布局容器对齐方式（eTS）:RK3568, 3.2Beta1, 展示容器的对齐方式
+ 闹钟应用（eTS）:RK3568, 3.1 Release, 展示OpenHarmony的后台代理能力
+ 视频播放器（eTS）:RK3568, 3.2Beta1, 展示VideoPlayer组件
+ 第一个Native C++应用（eTS）: RK3568, 3.1 Release, 展示Native接口使用
+ Native Component(eTS):RK3568, 3.1 Release,展示Xcomponent调用Native API
+ OLED屏幕显示： Hi3861, 3.0LTS, 展示轻量设备I2C/ADC
+ 智能门禁：Hi3516, 3.0LTS, 展示设备间通信
+ 使用UDP实心与服务端通信（eTS）: RK3568, 3.2Beta1, 展示UDP通信
+ 使用HTTP实现与服务端通信（eTS）:RK3568, 3.2Beta1, 展示HTTP通信
+ 使用TCP实现与服务端通信（eTS）:RK3568, 3.2Beta1, 展示TCP通信
+ 手势截屏（eTS）:RK3568, 3.2Beta1, 展示手势处理以及截屏能力
+ 字符串加解密（eTS）:RK3568, 3.2Beta1, 展示字符串加解密能力

**结论**  
同意该批次15个开发样例上官网的申请
#### 议题三、知识赋能版权讨论(孙鑫）
**进展** 
根据当前知识赋能可能使用的授权模板，知识赋能课程的相关材料所有权属于讲师和基金会；部分讲师是代表企业参与该项活动，而企业单位却无权使用相关内容，建议调整模板或者出一个针对企业讲师的授权模板。
**结论**  
相关工作组目前没有法务等职能；相关诉求单位可以一起向基金会法务提出相关诉求。   

#### 议题四、DevEco Device Tool赋能课程
**进展** 
针对DevEcoTools的使用做的系列赋能课程。
+ 课程形式:录制
+ 上线时间: 22年12月
+ 开发平台: DevEcoDeviceTool 3.1 Beta ubuntu/windows
+ 课程目标: 通过本期课程，开发者可以学到如何使用Device Tool进行OpenHarmony设备开发，结合OpenHarmony官网样例可以迅速上手
+ 课程设置：  
    + VirtualBox虚拟机的安装以及Ubuntu安装
    + Ubuntu20.04基础环境配置
    + 搭建DevEco Device Tool的开发环境
    + DevEco Device Tool功能总览
    + 一键拉取OpenHarmony工程
    + 导入/修改OpenHarmony工程
    + 创建工程详解
    + 自动创建产品化配置详解
    + 一键生成标准设备HDF通用驱动模板  
**结论**  
该课程设置有利社区开发者更快的掌握OpenHarmony设备开发环境搭建，很有价值，同意该课程设置，后续按照相关的审核流程在官网呈现。

[本次会议录播视频](https://pan.baidu.com/s/1Jq9H1ONksATA-vabwPkvTw?pwd=qgmx)

## Action items
